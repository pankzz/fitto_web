window.onload = function() {
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyDEdF5QDOqrnxrca5_JrbM4cKnGIhjm7Nk",
        authDomain: "fitto-395cf.firebaseapp.com",
        databaseURL: "https://fitto-395cf.firebaseio.com",
        storageBucket: "fitto-395cf.appspot.com",
    };

    firebase.initializeApp(config);

    FB.init({
        appId: '143308952773618',
        status: true,
        xfbml: true,
        version: 'v2.7'
    });

    FB.Event.subscribe('auth.authResponseChange', checkLoginState);

    //will work for first time page load and on reloads
    FB.getLoginStatus(function(response) {
        if (response.status === "connected") {
            //show him the format
        } else {
            $("#mainContent").html(loginTemplate);
        }
    });

};

var gamePlayerNumber = {
  football: 10,
  basketball: 10,
  pool: 4,
  badminton: 4,
  cricket: 10
};

var fromt = null;
var tot = null;

var thankyouTemplate = '<div calss="row">' +
    '<div class="col-md-12 text-center" style="margin-top:20px">' +
    '<div><i class="animated flipInX fa fa-thumbs-up fa-4x" aria-hidden="true"></i></div>' +
    '<div><p class="animated fadeIn lead">Sign Up successfull!'+
    // '<br/>You will be notified whenever there is a game nearby'+
    '</p></div>' +
    '<button class="btn btn-primary btn-lg" onclick="initCreateGameForm()">Create Game</button>' +
    '</div>' +
    '</div>';

var loginTemplate = '<div class="row">' +
    '<div class="text-center">' +
    '<h3 class="animated bounceIn">Sign Up - Join Beta Program</h3>' +
    '<p class="animated bounceIn lead">Be a member of Fitto family</p>' +
    '<button class="animated fadeIn btn btn-primary" onclick="authUser();"><i class="fa fa-facebook fa-fw" aria-hidden="true"> </i> Login</button>' +
    '</div>' +
    '</div>';

var registrationForm = '<div class="row" id="homeForm">' +
    '<div class="col-md-3 ">' +
    '' +
    '</div>' +
    '<div class="col-md-6 " style="padding:auto;margin:auto">' +
    '<div class="text-center" id="photoDiv"><img id="photo" class="img-circle" src="" width="81px" height="81px"></div>' +
    '<div class="well" style="">' +
    '<form class="form-horizontal">' +
    '<fieldset>' +
    '<legend><i class="fa fa-wpforms fa-fw" aria-hidden="true" style="padding-top:20px"> </i>Create your profile</legend>' +
    '<p id="msg" class="alert alert-danger"></p>' +
    '<div class="form-group">' +
    '<label for="inputName" class="col-md-4 control-label">Name *</label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputName">' +
    '</div>' +
    '</div>' +
    '<div class="form-group">' +
    '<label for="inputMobNo" class="col-md-4 control-label">Mobile No. </label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputMobNo" onchange="return validatePhoneNo();">' +
    '</div>' +
    '</div>' +
    '<div class="form-group">' +
    '<label for="inputEmail" class="col-md-4 control-label">Email *</label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputEmail" onchange="return validateEmailId();">' +
    '</div>' +
    '</div>' +

    '<div class="form-group">' +
    '<label for="selectLoc" class="col-md-4 control-label">Location *</label>' +
    '<div class="col-md-8">' +
    '<select class="form-control" id="selectLoc" >' +
    '<option value="">Choose your location</option>' +
    '<option value="aundh">Aundh</option>' +
    '<option value="balewadi">Balewadi</option>' +
    '<option value="baner">Baner</option>' +
    '<option value="deccan">Deccan</option>' +
    '<option value="hadapsar">Hadapsar</option>' +
    '<option value="hinjewadi">Hinjewadi</option>' +
    '<option value="kondhwa">Kondhwa</option>' +
    '<option value="kothrud">Kothrud</option>' +
    '<option value="magarpatta">Magarpatta </option>' +
    '<option value="mundhwa">Mundhwa</option>' +
    '<option value="pashan">Pashan</option>' +
    '<option value="pimple_saudagar">Pimple Saudagar</option>' +
    '<option value="pimpri_chinchwad">Pimpri Chinchwad</option>' +
    '<option value="shivaji_nagar">Shivaji Nagar</option>' +
    '<option value="swargate">Swargate</option>' +
    '<option value="viman_nagar">Viman Nagar</option>' +
    '<option value="wakad">Wakad</option>' +
    '<option value="wanowrie">Wanowrie</option>' +
    '</select>' +
    '</div>' +
    '</div>' +


    '<div class="form-group">' +
    '<label class="col-md-4 control-label">Interested in *</label>' +
    '<div class="col-md-8">' +
    '<div class="checkbox">' +
    '<label>' +
    '<input type="checkbox" value="football">Football' +
    '</label>' +
    '</div>' +
    '<div class="checkbox">' +
    '<label>' +
    '<input type="checkbox" value="pool">Pool' +
    '</label>' +
    '</div>' +
    '<div class="checkbox">' +
    '<label>' +
    '<input type="checkbox" value="badminton">Badminton' +
    '</label>' +
    '</div>' +
    '<div class="checkbox">' +
    '<label>' +
    '<input type="checkbox" value="cricket">Cricket' +
    '</label>' +
    '</div>' +
    '<div class="checkbox">' +
    '<label>' +
    '<input type="checkbox" value="basketball">Basketball' +
    '</label>' +
    '</div>' +
    '</div>' +
    '</div>' +
    'Note : You can change your interest(s) anytime later' +
    '</div>' +
    '<div class="form-group ">' +
    // '<div class="pull-right">' +
    '<button type="submit " class="btn btn-primary btn-block" onclick="return submitResponse(); "><i class="fa fa-paper-plane fa-fw" aria-hidden="true"> </i> SignUp</button>' +
    // '</div>' +
    '</div>' +
    '</fieldset>' +
    '</form>' +
    '</div>' +
    '</div>' +
    '<div class="col-md-3 ">' +
    '' +
    '</div>' +
    '</div>'

// '<button type="reset " class="btn btn-default " style="margin-right:10px">Cancel</button>' +

var createGameForm = '<div class="row" id="homeForm">' +
    '<div class="col-md-3">' +
    // 'Recommendation goes here...' +
    '</div>' +
    '<div class="col-md-6">' +
    '<div class="text-center" id="photoDiv"><img id="photo" class="img-circle" src="" width="81px" height="81px"></div>' +
    '<div class="well">' +
    '<form class="form-horizontal">' +
    '<fieldset>' +
    '<legend><i class="fa fa-wpforms fa-fw" aria-hidden="true" style="padding-top:20px"> </i>Create your game</legend>' +
    '<p id="msg" class="alert alert-danger"></p>' +
    '<div class="form-group">' +
    '<label for="inputName" class="col-md-4 control-label">Name</label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputName">' +
    '</div>' +
    '</div>' +
    '<div class="form-group">' +
    '<label for="inputMobNo" class="col-md-4 control-label">Mobile No.</label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputMobNo" onchange="return validatePhoneNo();">' +
    '</div>' +
    '</div>' +
    '<div class="form-group">' +
    '<label for="inputEmail" class="col-md-4 control-label">Email</label>' +
    '<div class="col-md-8">' +
    '<input type="text" class="form-control" id="inputEmail" onchange="return validateEmailId();">' +
    '</div>' +
    '</div>' +

    '<div class="form-group">' +
    '<label for="selectGame" class="col-md-4 control-label">Game</label>' +
    '<div class="col-md-8">' +
    '<select class="form-control" id="selectGame" onchange="return validatePlayerNumber()">' +
    '<option value="football">FootBall</option>' +
    '<option value="pool">Pool</option>' +
    '<option value="badminton">Badminton</option>' +
    '<option value="cricket">Cricket </option>' +
    '</select>' +
    '</div>' +
    '</div>' +
    '<div class="form-group">' +
    '<label for="selectLoc" class="col-md-4 control-label">Location</label>' +
    '<div class="col-md-8">' +
    '<select class="form-control" id="selectLoc" onchange="">' +
    '<option value="">Choose your location</option>' +
    '<option value="aundh">Aundh</option>' +
    '<option value="balewadi">Balewadi</option>' +
    '<option value="baner">Baner</option>' +
    '<option value="deccan">Deccan</option>' +
    '<option value="hadapsar">Hadapsar</option>' +
    '<option value="hinjewadi">Hinjewadi</option>' +
    '<option value="kondhwa">Kondhwa</option>' +
    '<option value="kothrud">Kothrud</option>' +
    '<option value="magarpatta">Magarpatta </option>' +
    '<option value="mundhwa">Mundhwa</option>' +
    '<option value="pashan">Pashan</option>' +
    '<option value="pimple_saudagar">Pimple Saudagar</option>' +
    '<option value="pimpri_chinchwad">Pimpri Chinchwad</option>' +
    '<option value="shivaji_nagar">Shivaji Nagar</option>' +
    '<option value="swargate">Swargate</option>' +
    '<option value="viman_nagar">Viman Nagar</option>' +
    '<option value="wakad">Wakad</option>' +
    '<option value="wanowrie">Wanowrie</option>' +
    '</select>' +
    '</div>' +
    '</div>' +

    '<div class="form-group ">' +
    '<label for="inputDate " class="col-md-4 control-label ">On</label>' +
    '<div class="col-md-8 " id="inputDatePicker ">' +
    '<input type="text " data-date-format="dd M, yy " class="datepicker form-control " id="inputDate">' +
    '</div>' +
    '</div>' +
    '<div class="form-group ">' +
    '<label for="inputfrmTime " class="col-md-4 control-label ">At</label>' +
    '<div class="col-md-4 bootstrap-timepicker timepicker ">' +
    '<input type="text " class="form-control " id="inputfrmTime" placeholder="from " onchange="return validateTime(); ">' +
    '</div>' +
    '<div class="col-md-4 bootstrap-timepicker timepicker ">' +
    '<input type="text " class="form-control " id="inputtoTime" placeholder="to " onchange="return validateTime();">' +
    '</div>' +
    '</div>' +
    '<div class="form-group ">' +
    '<label for="inputWEA " class="col-md-4 control-label ">We are already</label>' +
    '<div class="col-md-8 ">' +
    '<input type="text " class="form-control " id="inputWEA" placeholder="e.g: 2 players " onchange="return validatePlayerNumber(); " value="1">' +
    '</div>' +
    '</div>' +
    '<div class="form-group ">' +
    '<label for="inputLO " class="col-md-4 control-label ">Looking for</label>' +
    '<div class="col-md-8 ">' +
    '<input type="text " class="form-control " id="inputLO" disabled value="9">' +
    '</div>' +
    '</div>' +
    '<div class="form-group ">' +
    // '<div class="pull-right">' +
    // '<button type="reset " class="btn btn-default " style="margin-right:10px">Cancel</button>' +
    '<button type="submit " class="btn btn-primary btn-block" style="margin-right:10px" onclick="return submitGameResponse(); ">Submit</button>' +
    // '</div>' +
    // '</div>' +
    '</fieldset>' +
    '</form>' +
    '<div class="col-md-3">'+
    ''+
    '</div>'
    '</div>'

var thankYouAndPayment = '<div calss="row">' +
    '<div class="col-md-12 text-center" style="margin-top:20px">' +
    '<div><i class="animated flipInX fa fa-thumbs-up fa-4x" aria-hidden="true"></i></div>' +
    '<div><p class="animated fadeIn lead">Game resgistration successfull!<br/>You will be notified for payment whenever your event is confirmed</p></div>' +
    // '<button class="btn btn-primary" onclick="initCreateGameForm()">Create Game</button>' +
    '</div>' +
    '</div>';

function submitGameResponse() {

  var fromTime = $("#inputfrmTime").val();
  var toTime = $("#inputtoTime").val();

  if ($("#inputName").val().length <= 0) {
      $("#msg").text("Please enter your name");
      $("#msg").show();
  } else if ($("#inputEmail").val().length <= 0) {
      $("#msg").text("Please enter your email-id");
      $("#msg").show();
  }
  else if( $("#inputMobNo").val().length <= 0 ) {
      $("#msg").text("Please enter your mobile number");
      $("#msg").show();
  }
  else if ($("#selectLoc").val().length <= 0) {
      $("#msg").text("Please enter location");
      $("#msg").show();
  } else if ($("#inputWEA").val().trim().length <= 0) {
    $("#msg").text("Please enter we are already");
    $("#msg").show();
  } else if ($("#inputLO").val().trim().length <= 0) {
    $("#msg").text("Please enter looking for");
    $("#msg").show();
  } else if (Number($("#inputWEA").val().trim()) + Number($("#inputLO").val().trim()) !==  gamePlayerNumber[$("#selectGame").val()]) {

    console.log(Number($("#inputWEA").val().trim()) + Number($("#inputLO").val().trim()));
    console.log(gamePlayerNumber[$("#selectGame").val()]);
    $("#msg").text("Please enter valid value for we are already");
    $("#msg").show();
  } else if(moment(fromTime, "hh:mm A").isBefore(moment(fromt,"hh:mm A")) || moment(toTime, "hh:mm A").isAfter(moment(tot,"hh:mm A"))) {
      $("#msg").show();
      $("#msg").text("From time shoule be in " + fromt + " - " + tot);
  } else if(moment(fromTime, "hh:mm A").isAfter(moment(toTime,"hh:mm A")) || moment(fromTime, "hh:mm A").isSame(moment(toTime,"hh:mm A"))){
    $("#msg").show();
    $("#msg").text("Invalid time");
  } else {
      //everything is perfect
      //console.log("perfecto...")
      firebase.database().ref('events').push({
        name: $("#inputName").val(),
        email: $("#inputEmail").val(),
        mob: $("#inputMobNo").val(),
        loc: $("#selectLoc").val(),
        game: $("#selectGame").val(),
        date: $("#inputDate").val(),
        wea: $("#inputWEA").val(),
        lo: $("#inputLO").val(),
        fromtime: $("#inputfrmTime").val(),
        totime: $("#inputtoTime").val()
      });

      $("#mainContent").html(thankYouAndPayment);
  }

  return false;
}

function initCreateGameForm(name,email,mob,loc) {
    //Pull all the data from the firebase and update the form
    firebase.database().ref('signup/' + firebase.auth().currentUser.uid).once('value').then(function(snap) {
      if(snap.val() == null) {
        console.log("user is not signed up....this will not be possible");
      } else {
          $("#inputName").val(snap.val().name);
          $("#inputEmail").val(snap.val().email);
          $("#inputMobNo").val(snap.val().mob);
          $("#selectLoc").val(snap.val().loc);
      }
    });

    firebase.database().ref('config').once('value').then(function(snap) {
      if(snap.val() == null) {
        console.log("We don't have any config");
      } else {

          $('#inputfrmTime').timepicker({
              showSeconds: false,
              minuteStep: snap.val().minstep,
              // template: false
              defaultTime: snap.val().fromtime
          });

          $('#inputtoTime').timepicker({
              showSeconds: false,
              minuteStep: snap.val().minstep,
              defaultTime: snap.val().totime
              // template: false
          });

          $("#inputDate").datepicker({
              endDate: ''+snap.val().advbook,
              startDate: '+1d'
          });

          fromt = snap.val().fromtime;
          tot = snap.val().totime;

          $("#inputfrmTime").val(fromt);
          $("#inputtoTime").val(tot);
          $("#inputDate").datepicker('update', new Date()+1);

      }
    });
    $("#mainContent").html(createGameForm);
    $("#msg").hide();
    $("#photo").attr("src", firebase.auth().currentUser.photoURL);
    $("#fblog").html('<a href="#" onclick="unauthUser();"><span class="fa fa-facebook fa-fw" aria-hidden="true"></span>LogOut</a>');
}

function authUser() {
    FB.login(checkLoginState, { scope: 'public_profile,email' });
}

function checkLoginState(event) {
    if (event.authResponse) {
        console.log("User is signed-in Facebook.");
        var unsubscribe = firebase.auth().onAuthStateChanged(function(firebaseUser) {
            unsubscribe();
            console.log("Check if we are already signed-in Firebase with the correct user.");
            if (!isUserEqual(event.authResponse, firebaseUser)) {
                console.log("Build Firebase credential with the Facebook auth token.");
                var credential = firebase.auth.FacebookAuthProvider.credential(
                    event.authResponse.accessToken);
                console.log("Sign in with the credential from the Facebook user.");
                firebase.auth().signInWithCredential(credential).catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    var email = error.email;
                    var credential = error.credential;
                    // ...
                }).then(function(user) {
                    //check user is already signup
                    firebase.database().ref('signup/' + firebase.auth().currentUser.uid).once('value').then(function(snap) {
                      if(snap.val() == null) {
                        console.log("user is not signed up....this will not be possible");
                        $("#mainContent").html(registrationForm);
                        console.log(user.photoURL);
                        initForm(user.displayName, user.email, user.photoURL)
                      } else {
                          initCreateGameForm();
                      }
                    });

                });


            } else {
                console.log("User is already signed-in Firebase with the correct user");
                //TODO Logic for showing the form
                firebase.database().ref('signup/' + firebase.auth().currentUser.uid).once('value').then(function(snap) {
                  if(snap.val() == null) {
                    console.log("user is not signed up....this will not be possible");
                    $("#mainContent").html(registrationForm);
                    console.log(firebase.photoURL);
                    initForm(firebaseUser.displayName, firebaseUser.email, firebaseUser.photoURL);
                  } else {
                      initCreateGameForm();
                  }
                });
                // $("#mainContent").html(registrationForm);
                // console.log(firebase.photoURL);
                // initForm(firebaseUser.displayName, firebaseUser.email, firebaseUser.photoURL);
            }
        });
    } else {
        // User is signed-out of Facebook.
        firebase.auth().signOut();
        $("#mainContent").html(loginTemplate);
        $("#fblog").html('<a href="#" onclick="authUser();"><span class="fa fa-facebook fa-fw" aria-hidden="true"></span>LogIn</a>');
    }
}

function isUserEqual(facebookAuthResponse, firebaseUser) {
    if (firebaseUser) {
        var providerData = firebaseUser.providerData;
        for (var i = 0; i < providerData.length; i++) {
            if (providerData[i].providerId === firebase.auth.FacebookAuthProvider.PROVIDER_ID &&
                providerData[i].uid === facebookAuthResponse.userID) {
                // We don't need to re-auth the Firebase connection.
                return true;
            }
        }
    }
    return false;
}

function unauthUser() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout(function(response) {

            });
        }
    });
}

function initForm(name, email, photoURL) {
    $("#inputName").val(name);
    $("#inputEmail").val(email);
    $("#msg").hide();
    $("#photo").attr("src", photoURL);
    //Logout displayName
    $("#fblog").html('<a href="#" onclick="unauthUser();"><span class="fa fa-facebook fa-fw" aria-hidden="true"></span>LogOut</a>');
}

function validatePhoneNo() {

    if (!/^(0|91|\+91)?[789][0-9]{9}$/.test($("#inputMobNo").val())) {
        $("#msg").show();
        $("#msg").text("Mobile Number " + $("#inputMobNo").val() + " is not valid");
        $("#inputMobNo").val('');
    }
}

function validateEmailId() {
    if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#inputEmail").val())) {
        $("#msg").show();
        $("#msg").text("Email id " + $("#inputEmail").val() + " is not valid");
        $("#inputEmail").val('');
    }
}

function validateTime(source) {

    var fromTime = $("#inputfrmTime").val();
    var toTime = $("#inputtoTime").val();
    // if (source === "from") {

        if(moment(fromTime, "hh:mm A").isBefore(moment(fromt,"hh:mm A")) || moment(toTime, "hh:mm A").isAfter(moment(tot,"hh:mm A"))) {
            $("#msg").show();
            $("#msg").text("From time shoule be in " + fromt + " - " + tot);
            // $("#inputfrmTime").val(fromt);
        }
        else if(moment(fromTime, "hh:mm A").isAfter(moment(toTime,"hh:mm A")) || moment(fromTime, "hh:mm A").isSame(moment(toTime,"hh:mm A"))){
          console.log(fromTime);
          console.log(toTime);

          $("#msg").show();
          $("#msg").text("Invalid time");
          // $("#inputtoTime").val(tot);
        }

        if(moment(fromTime, "hh:mm A").isBefore(moment(tot,"hh:mm A"))) {
         if(moment(fromTime, "hh:mm A").minutes() > 0 || moment(toTime, "hh:mm A").minutes() > 0) {
           $("#inputfrmTime").val(moment(fromTime, "hh:mm A").minutes(0).format("hh:mm A"));
           $("#inputtoTime").val(moment(toTime, "hh:mm A").minutes(0).format("hh:mm A"));
           // $("#inputtoTime").val(moment(fromTime, "hh:mm A").add(moment.duration(1,'hours')).format("hh:mm A"));
         }
       }
    // }

        // if (moment(fromTime, "hh:mm A").minutes() > 0 && moment(fromTime, "hh:mm A").minutes() <= 15)
        //     $("#inputfrmTime").val(moment(fromTime, "hh:mm A").minutes(30).format("hh:mm A"));
        // else if (moment(fromTime, "hh:mm A").minutes() > 15 && moment(fromTime, "hh:mm A").minutes() < 30)
        //     $("#inputfrmTime").val(moment(fromTime, "hh:mm A").minutes(30).format("hh:mm A"));
        // else if (moment(fromTime, "hh:mm A").minutes() > 30 && moment(fromTime, "hh:mm A").minutes() <= 45)
        //     $("#inputfrmTime").val(moment(fromTime, "hh:mm A").minutes(60).format("hh:mm A"));
        // else if (moment(fromTime, "hh:mm A").minutes() > 45)
        //     $("#inputfrmTime").val(moment(fromTime, "hh:mm A").minutes(60).format("hh:mm A"));
    //
    // } else {
    //
    //     if (moment(toTime, "hh:mm A").minutes() < 0 && moment(toTime, "hh:mm A").minutes() <= 15)
    //         $("#inputtoTime").val(moment(toTime, "hh:mm A").minutes(30).format("hh:mm A"));
    //     else if (moment(toTime, "hh:mm A").minutes() > 15 && moment(toTime, "hh:mm A").minutes() < 30)
    //         $("#inputtoTime").val(moment(toTime, "hh:mm A").minutes(30).format("hh:mm A"));
    //     else if (moment(toTime, "hh:mm A").minutes() > 30 && moment(toTime, "hh:mm A").minutes() <= 45)
    //         $("#inputtoTime").val(moment(toTime, "hh:mm A").minutes(60).format("hh:mm A"));
    //     else if (moment(toTime, "hh:mm A").minutes() > 45)
    //         $("#inputtoTime").val(moment(toTime, "hh:mm A").minutes(60).format("hh:mm A"));
    // }
    //
    // if (!moment($("#inputfrmTime").val(), "hh:mm A").isBefore(moment($("#inputtoTime").val(), "hh:mm A"))) {
    //     $("#inputtoTime").val(moment($("#inputfrmTime").val(), "hh:mm A").add('m', 30).format("hh:mm A"));
    // }
    // recommendation();
    return true;
}

function validatePlayerNumber() {

    if (!/^[1-9][0-9]*$/.test($("#inputWEA").val().trim().replace(/^0+/, '')) || Number($("#inputWEA").val().trim()) > gamePlayerNumber[$("#selectGame").val()] ) {
        $("#msg").show();
        $("#msg").text("We are already value " + $("#inputWEA").val() + " is not valid");
        $("#inputWEA").val(1);

    } else {
      $("#inputLO").val(gamePlayerNumber[$("#selectGame").val()] - Number($("#inputWEA").val()));
    }
    // if (!/^[1-9][0-9]*$/.test($("#inputLO").val().replace(/^0+/, ''))) {
    //     $("#msg").show();
    //     $("#msg").text("Looking for value " + $("#inputLO").val() + " is not valid");
    //     $("#inputLO").val(1);
    // }
    return true;
}

function validatePhoneNo() {

    if (!/^(0|91|\+91)?[789][0-9]{9}$/.test($("#inputMobNo").val())) {
        $("#msg").show();
        $("#msg").text("Mobile Number " + $("#inputMobNo").val() + " is not valid");
        $("#inputMobNo").val('');
    }
}

function submitResponse() {

    //Validate all the stuffs here....
    if ($("#inputName").val().length <= 0) {
        $("#msg").text("Please enter your name");
        $("#msg").show();
    } else if ($("#inputEmail").val().length <= 0) {
        $("#msg").text("Please enter your email-id");
        $("#msg").show();
    }
    // else if( $("#inputMobNo").val().length <= 0 ) {
    //     $("#msg").text("Please enter your mobile number");
    //     $("#msg").show();
    // }
    else if ($("#selectLoc").val().length <= 0) {
        $("#msg").text("Please enter location");
        $("#msg").show();
    } else if ($(":checkbox:checked").length <= 0) {
        $("#msg").text("Please select atleast one game of interest");
        $("#msg").show();
    } else {
        $("#msg").hide();

        var selectedGames = $.map($(':checkbox:checked'), function(input) {
            return $(input).val();
        }).join(",");

        firebase.database().ref('signup/' + firebase.auth().currentUser.uid).set({

            name: $("#inputName").val(),
            email: $("#inputEmail").val(),
            mob: $("#inputMobNo").val(),
            loc: $("#selectLoc").val(),
            games: selectedGames,
            date: Date()
        });

        $("#mainContent").html(thankyouTemplate);

        return false;
    }

    return false;
}
