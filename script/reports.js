window.onload = function() {
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyDEdF5QDOqrnxrca5_JrbM4cKnGIhjm7Nk",
        authDomain: "fitto-395cf.firebaseapp.com",
        databaseURL: "https://fitto-395cf.firebaseio.com",
        storageBucket: "fitto-395cf.appspot.com",
    };

    firebase.initializeApp(config);

    FB.init({
        appId: '143308952773618',
        status: true,
        xfbml: true,
        version: 'v2.7'
    });

    FB.Event.subscribe('auth.authResponseChange', checkLoginState);

    //will work for first time page load and on reloads
    FB.getLoginStatus(function(response) {
        if (response.status === "connected") {
            //show him the format
        } else {
            $("#mainContent").html(loginTemplate);
        }
    });

};

function authUser() {
    FB.login(checkLoginState, { scope: 'public_profile,email' });
}

var loginTemplate = '<div class="row">' +
    '<div class="col-lg-8 col-md-7 col-sm-6">' +
    '<h1>Welcome</h1>' +
    '<p class="lead">Please login with Facebook</p>' +
    '<button class="btn btn-primary" onclick="authUser();">Login</button>' +
    '</div>' +
    '</div>';


function checkLoginState(event) {
    if (event.authResponse) {
        console.log("User is signed-in Facebook.");
        var unsubscribe = firebase.auth().onAuthStateChanged(function(firebaseUser) {
            unsubscribe();
            console.log("Check if we are already signed-in Firebase with the correct user.");
            if (!isUserEqual(event.authResponse, firebaseUser)) {
                console.log("Build Firebase credential with the Facebook auth token.");
                var credential = firebase.auth.FacebookAuthProvider.credential(
                    event.authResponse.accessToken);
                console.log("Sign in with the credential from the Facebook user.");
                firebase.auth().signInWithCredential(credential).catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    var email = error.email;
                    var credential = error.credential;
                    // ...
                }).then(function(user) {
                    //TODO
                    firebase.database().ref('signup').on('value', function(snapshot) {
                        if (snapshot.val() != null) {

                            var buildTable = "<table class='table table-striped table-hover'><thead><tr><th>Date</th><th>Name</th><th>Email</th><th>Mobile No</th><th>Location</th><th>Games</th></tr></thead>";
                            var regs = snapshot.val();
                            for (reg in regs) {
                                buildTable = buildTable + "<tr><td>";
                                buildTable = buildTable + regs[reg].date;
                                buildTable = buildTable + "</td><td>";
                                buildTable = buildTable + regs[reg].name;
                                buildTable = buildTable + "</td><td>";
                                buildTable = buildTable + regs[reg].email;
                                buildTable = buildTable + "</td><td>";
                                buildTable = buildTable + regs[reg].mob;
                                buildTable = buildTable + "</td><td>";
                                buildTable = buildTable + regs[reg].loc;
                                buildTable = buildTable + "</td><td>";
                                buildTable = buildTable + regs[reg].games;
                                buildTable = buildTable + "</td></tr>";
                            }
                            buildTable = buildTable + "</table>";



                            $("#signup").html(buildTable);
                        } else {
                            $("#signup").text("No registrations yet");
                        }
                    });


                    firebase.database().ref('events').on('value',function(snap) {
                        console.log(snap.val());
                        if (snap.val() != null) {

                          var buildTable = "<table class='table table-striped table-hover'><thead><tr><th>Date</th><th>From Time</th><th>To Time</th><th>Name</th><th>Email</th><th>Mobile No</th><th>Location</th><th>Game</th><th>WEA</th><th>LO</th></tr></thead>";
                          var regs = snap.val();
                          for (reg in regs) {
                              buildTable = buildTable + "<tr><td>";
                              buildTable = buildTable + regs[reg].date;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].fromtime;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].totime;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].name;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].email;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].mob;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].loc;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].game;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].wea;
                              buildTable = buildTable + "</td><td>";
                              buildTable = buildTable + regs[reg].lo;
                              buildTable = buildTable + "</td></tr>";
                          }
                          buildTable = buildTable + "</table>";
                          $("#games").html(buildTable);
                        } else {
                            $("#games").text("No registrations yet");
                        }
                    });

                });


            } else {
                console.log("User is already signed-in Firebase with the correct user");
                //TODO
                firebase.database().ref('signup').on('value', function(snapshot) {
                    if (snapshot.val() != null) {

                        var buildTable = "<table class='table table-striped table-hover'><thead><tr><th>Date</th><th>Name</th><th>Email</th><th>Mobile No</th><th>Location</th><th>Games</th></tr></thead>";
                        var regs = snapshot.val();
                        for (reg in regs) {
                            buildTable = buildTable + "<tr><td>";
                            buildTable = buildTable + regs[reg].date;
                            buildTable = buildTable + "</td><td>";
                            buildTable = buildTable + regs[reg].name;
                            buildTable = buildTable + "</td><td>";
                            buildTable = buildTable + regs[reg].email;
                            buildTable = buildTable + "</td><td>";
                            buildTable = buildTable + regs[reg].mob;
                            buildTable = buildTable + "</td><td>";
                            buildTable = buildTable + regs[reg].loc;
                            buildTable = buildTable + "</td><td>";
                            buildTable = buildTable + regs[reg].games;
                            buildTable = buildTable + "</td></tr>";
                        }
                        buildTable = buildTable + "</table>";
                        $("#signup").html(buildTable);
                    } else {
                        $("#signup").text("No registrations yet");
                    }
                });

                firebase.database().ref('events').on('value',function(snap) {
                    console.log(snap.val());
                    if (snap.val() != null) {

                      var buildTable = "<table class='table table-striped table-hover'><thead><tr><th>Date</th><th>From Time</th><th>To Time</th><th>Name</th><th>Email</th><th>Mobile No</th><th>Location</th><th>Game</th><th>WEA</th><th>LO</th></tr></thead>";
                      var regs = snap.val();
                      for (reg in regs) {
                          buildTable = buildTable + "<tr><td>";
                          buildTable = buildTable + regs[reg].date;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].fromtime;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].totime;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].name;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].email;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].mob;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].loc;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].game;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].wea;
                          buildTable = buildTable + "</td><td>";
                          buildTable = buildTable + regs[reg].lo;
                          buildTable = buildTable + "</td></tr>";
                      }
                      buildTable = buildTable + "</table>";
                      $("#games").html(buildTable);
                    } else {
                        $("#games").text("No registrations yet");
                    }
                });

            }
        });
    } else {
        // User is signed-out of Facebook.
        firebase.auth().signOut();
        $("#mainContent").html(loginTemplate);
        $("#fblog").html('<a href="#" onclick="authUser();"><span class="fa fa-facebook fa-fw" aria-hidden="true"></span>LogIn</a>');
    }
}

function isUserEqual(facebookAuthResponse, firebaseUser) {
    if (firebaseUser) {
        var providerData = firebaseUser.providerData;
        for (var i = 0; i < providerData.length; i++) {
            if (providerData[i].providerId === firebase.auth.FacebookAuthProvider.PROVIDER_ID &&
                providerData[i].uid === facebookAuthResponse.userID) {
                // We don't need to re-auth the Firebase connection.
                return true;
            }
        }
    }
    return false;
}


function unauthUser() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout(function(response) {

            });
        }
    });
}
